function customRound(number, roundUp) {
    if (roundUp) {
        return Math.ceil(number); 
    } else {
        return Math.floor(number); 
    }
}


let inputNumber = 3.14;
let shouldRoundUp = true; 
let roundedNumber = customRound(inputNumber, shouldRoundUp);

if (shouldRoundUp) {
    console.log("Rounded up number: " + roundedNumber);
} else {
    console.log("Rounded down number: " + roundedNumber); 
}
